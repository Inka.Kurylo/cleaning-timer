const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
    function list (array,parent = document.body) {
        let li;
        let ul;
        ul = document.createElement('ol');
        parent.append(ul);
        let res = array.map((value) => {
            if (Array.isArray(value)){
              li = document.createElement('li');
              ul.append(li);
              list (value, li);
            } else {
              li = document.createElement('li');
              li.innerText = value;
              ul.append(li);}
            
        });
    };
    let div = document.createElement('div');
    div.id ='timer';
    document.body.append(div); 
    list(arr);
    let secLast = 4;
    function mlSec(){
      secLast--;
      timer.innerText = 'Screen cleaning: '+ secLast;
      if(secLast != 0){
        setTimeout(mlSec, 1000);
        } else {
        document.body.remove();
        };
    };
    mlSec();